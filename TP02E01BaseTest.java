package tp2;

import static org.junit.Assert.*;

import org.junit.Test;

import atelier5.AT05E01Base;

public class TP02E01BaseTest {

	@Test
	public void testFusionVec() {
	
		// ----- DONNEES VALIDES -----
		// Tableaux tries avec toutes les cases remplies
		int[] vecTriePleinA1 = { 1, 4, 5 };
		int[] vecTriePleinB1 = { 4, 6, 7, 8 };
		
		// Appeler la methode fusionVec() pour creer le tableau fusion
		int[] vecTrieFusion1 = null;
		vecTrieFusion1 = TP02E01Base.fusionVec(vecTriePleinA1, vecTriePleinB1); 

		// verifier le tableau obtenu apres fusion et faire le test
		int[] vecTrieVerifFusion1 = { 1, 4, 4, 5, 6, 7, 8};
		assertArrayEquals(vecTrieVerifFusion1, vecTrieFusion1);
		
	}
	
	@Test
	public void testExtraitValeurs() {
		
		//TEST PAIR
		int[] vecTriePleinA1 = {1, 2, 3, 4};
		int[] vecTrieREPA1 = {2, 4};
		int[] vecTrieVerifA1 = null;
		vecTrieVerifA1 = TP02E01Base.extraitValeurs(vecTriePleinA1, true); 
		assertArrayEquals(vecTrieVerifA1, vecTrieREPA1);
		
		//TEST IMPAIR
		int[] vecTriePleinB1 = {1, 2, 3, 4};
		int[] vecTrieREPB1 = {1, 3};
		int[] vecTrieVerifB1 = null;
		vecTrieVerifB1 = TP02E01Base.extraitValeurs(vecTriePleinB1, false); 
		assertArrayEquals(vecTrieVerifB1, vecTrieREPB1);
		
	}

}
