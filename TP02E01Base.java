package tp2;

import java.util.*;

public class TP02E01Base {
	public static final Random rnd = new Random(0);

	public static void main(String[] args) {
		Scanner clavier = new Scanner(System.in);
		System.out.print("Entrez la valeur maximale : ");
		int valMax = clavier.nextInt();
		System.out.print("Inscrivez le nombre de valeurs: ");
		int nbVal = clavier.nextInt();
		int[] vecOri = genereVecAleatoire(nbVal, valMax);
		System.out.print("Vecteur initial: ");
		afficheVecteur(vecOri);
		
		int[] vecImpairs = extraitValeurs(vecOri, false);
		int[] vecPairs = extraitValeurs(vecOri, true);
		System.out.print("Vecteur pairs: ");
		afficheVecteur(vecPairs);
		System.out.print("Vecteur impairs: ");
		afficheVecteur(vecImpairs);
		System.out.println();

		triSel(vecPairs);
		triSel(vecImpairs);
		System.out.println("Apres le tri de chaque vecteur:");		
		System.out.print("Vecteur pairs: ");
		afficheVecteur(vecPairs);
		System.out.print("Vecteur impairs: ");
		afficheVecteur(vecImpairs);
		System.out.println();

		int[] vecFusion = fusionVec(vecPairs, vecImpairs);
		System.out.print("Fusion des vecteurs:");
		afficheVecteur(vecFusion);
		System.out.println();
		
		System.out.println("Statistique des vecteurs (Min - Med - Max): ");
		System.out.println("Vecteur pairs: " + genereMinMedMaxStr(vecPairs));
		System.out.println("Vecteur impairs: " + genereMinMedMaxStr(vecImpairs));
		System.out.println("Fusion des vecteurs: " + genereMinMedMaxStr(vecFusion));
		System.out.println();
		
		System.out.print("Quelle est la valeur recherchee? ");
		int valCherchee = clavier.nextInt();
		int posVal = rechercheBin(vecFusion, valCherchee);
		if (posVal!=-1) {
			System.out.println("La valeur recherchee se trouve en position " + posVal);
		} else {
			System.out.print("Valeur introuvable");
		}
	}
	
	/**
	 * Permet de generer un vecteur de nombres aleatoires
	 * @param taille Taille du vecteur a generer
	 * @param valMax Valeur maximale des nombres generes.  Les
	 * 			valeurs seront entre 0 inclusif et valMax exclusif.
	 * @return Vecteur contenant les valeurs generees
	 */
	public static int[] genereVecAleatoire(int taille, int valMax) {
		int[] vecA = new int[taille];
		for (int i = 0; i < taille; ++i) {
			vecA[i] = Math.floorMod(rnd.nextInt(),valMax);
		}
		return vecA;
	}

	/**
	 * Fonction qui permet d'afficher un vecteur en le delimitant
	 * avec des crochets et en separant les valeurs par des virgules
	 * Ex: [1, 3, 5, 7, 9]
	 * @param vec Vecteur a afficher
	 */
	public static void afficheVecteur(int[] vec) {
		System.out.print("[");
		if (vec != null && vec.length > 0) {
			System.out.print(vec[0]);
			for (int i = 1; i < vec.length; ++i) {
				System.out.print(" ," + vec[i]);
			}
		}
		System.out.println("]");
	}

	/**
	 * Produit une chaine de caractere contenant le minimum, la mediane et
	 * le maximum du vecteur trie recu en parametre
	 * Exemple: Pour le vecteur [ 1, 3, 5, 7, 9] la chaine retournee sera
	 * "1 - 5 - 9" sans les guillements.
	 * @param vec Vecteur trie qui sera analyse
	 * @return Chaine indiquant, dans l'ordre, le min, la med et le max.
	 */
	public static String genereMinMedMaxStr(int[] vec) {
		String mmmStr = "" + vec[0] + " - ";
		if (vec.length%2 == 0) { // Nombre de cases pair
	        mmmStr += (vec[vec.length/2-1] + vec[vec.length/2])/2.0f;
	    } else { // Nombre de cases impair
	    	mmmStr += vec[vec.length/2];
	    }
		mmmStr += " - " + vec[vec.length-1];
		return mmmStr;
	}

	/**
	 * Cree un nouveau vecteur contenant seulement les valeurs paires ou
	 * impaires a partir du vecteur recu en parametre
	 * La taille du nouveau vecteur doit correspondre au nombre de valeurs extraites
	 * @param vecNb Vecteur de depart
	 * @param pair Selectionne les valeurs paires (true) ou impaires (false)
	 * @return Nouveau vecteur contenant les valeurs extraites
	 */
	public static int[] extraitValeurs(int[] vecNb, boolean pair) {
		int[] nbExtraits = null;
		// TODO: Ecrire des tests unitaires
		int compteur = 0;
		if (pair) { //SI PAIR
			for (int i = 0; i < vecNb.length; i++) {
				if (vecNb[i] % 2 == 0) {
					compteur++;
				}
			}
			nbExtraits = new int[compteur];
			compteur = 0;
			for (int i = 0; i < vecNb.length; i++) {
				if (vecNb[i] % 2 == 0) {
					nbExtraits[compteur] = vecNb[i];
					compteur++;
				}
			}
		} else { //SI IMPAIR
			for (int i = 0; i < vecNb.length; i++) {
				if (vecNb[i] % 2 != 0) {
					compteur++;
				}
			}
			nbExtraits = new int[compteur];
			compteur = 0;
			for (int i = 0; i < vecNb.length; i++) {
				if (vecNb[i] % 2 != 0) {
					nbExtraits[compteur] = vecNb[i];
					compteur++;
				}
			}
		}
		return nbExtraits;
	}
	
	/**
	 * Effectue un tri par selection du vecteur recu en parametre
	 * @param vec Vecteur qui sera trie
	 */
	public static void triSel(int[] vec) {
		// TODO: Ecrire des tests unitaires
		int posMin;
		int temp;
		for (int i = 0; i < vec.length; ++i) {
			posMin = i;
			for (int j = i+1; j < vec.length; ++j) {
				if (vec[j] < vec[posMin]) {
					posMin = j;
				}
			}
			if (posMin != i) {
				temp = (int)vec[i];
				vec[i] = vec[posMin];
				vec[posMin] = temp;
			}
		}
	}

	/**
	 * Fusionne deux vecteurs tries.  La taille du vecteur resultant
	 * est la somme des tailles des deux vecteurs recus en parametre
	 * @param vecA Premier vecteur trie a fusionner
	 * @param vecB Second vecteur trie a fusionner
	 * @return Vecteur trie resultant de la fusion de vecA et vecB
	 */
	public static int[] fusionVec(int[] vecA, int[] vecB) {
		// Vous devez creer un vecteur pour contenir le resultat de la fusion
		int[] vecF = new int[vecA.length + vecB.length];
		int posA = 0, posB = 0, posF = 0;
		triSel(vecA);
		triSel(vecB);
		while (posA < vecA.length && posB < vecB.length) {
			if (vecA[posA] < vecB[posB]) {
				vecF[posF] = vecA[posA];
				posA++;
			}
			else {
				vecF[posF] = vecB[posB];
				posB++;
			}
			posF++;
		}
		while (posA < vecA.length)
			vecF[posF++] = vecA[posA++];
		while (posB < vecB.length)
			vecF[posF++] = vecB[posB++];
		while (posF < vecF.length)
			vecF[posF++] = -1;
		return vecF;
	}
	
	/**
	 * Recherche binaire d'une valeur dans un vecteur trie
	 * @param vec Vecteur trie dans lequel la valeur sera recherchee
	 * @param valeur Valeur a rechercher
	 * @return Position de la valeur dans le vecteur ou -1 si non trouvee
	 */
	public static int rechercheBin(int[] vec, int valeur) {
		int posGa = 0;
		int posDr = vec.length - 1;
		int posMi = (posGa + posDr) / 2;
		while (posGa <= posDr && valeur != vec[posMi]) {
			if (valeur < vec[posMi])
				posDr = posMi - 1;
			else
				posGa = posMi + 1;
			posMi = (posGa + posDr) / 2;
		}
		if (posGa <= posDr)
			return posMi;
		else
			return -1;
	}
}
