package tp2;

import static org.junit.Assert.*;

import org.junit.Test;

public class TP02E02BaseTest {

	@Test
	public void testNbFinSemaines() {
		int[][] mois = TP02E02Base.genereMois(4, 2009);
		assertEquals(4, TP02E02Base.nbFinSemaines(mois));
		
		mois = TP02E02Base.genereMois(5, 2009);
		assertEquals(5, TP02E02Base.nbFinSemaines(mois));
	}

	@Test
	public void testTrouveJour() {
		int[][] mois = TP02E02Base.genereMois(4, 2009);
		
		assertEquals(6, TP02E02Base.trouveJour(mois, 1, 1));
		
		assertEquals(17, TP02E02Base.trouveJour(mois, 5, -2));
	}
}
